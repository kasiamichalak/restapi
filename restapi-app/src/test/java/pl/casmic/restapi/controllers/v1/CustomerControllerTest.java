package pl.casmic.restapi.controllers.v1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.casmic.restapi.model.CustomerDTO;
import pl.casmic.restapi.services.CustomerService;
import pl.casmic.restapi.services.ResourceNotFoundException;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CustomerControllerTest extends AbstractRestControllerTest {

    public static final String FIRSTNAME1 = "Firstname1";
    public static final String FIRSTNAME2 = "Firstname2";
    public static final String LASTNAME1 = "Lastname1";
    public static final String LASTNAME2 = "Lastname2";
    public static final Long ID1 = 1L;
    public static final Long ID2 = 2L;
    public static final String UPDATED_NAME = "UpdatedName";
    public static final String URL1 = CustomerController.BASE_URL + "/" + ID1;
    public static final String URL2 = CustomerController.BASE_URL + "/" + ID2;

    @Mock
    CustomerService customerService;
    @InjectMocks
    CustomerController customerController;
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController)
                .setControllerAdvice(RestResponseEntityExceptionHandler.class)
                .build();
    }

    @Test
    void testGetAllCustomers() throws Exception {

        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setFirstName(FIRSTNAME1);
        customerDTO1.setLastName(LASTNAME1);
        customerDTO1.setCustomerUrl(URL1);

        CustomerDTO customerDTO2 = new CustomerDTO();
        customerDTO2.setFirstName(FIRSTNAME2);
        customerDTO2.setLastName(LASTNAME2);
        customerDTO2.setCustomerUrl(URL2);

        List<CustomerDTO> customers = Arrays.asList(customerDTO1, customerDTO2);

        when(customerService.getAllCustomers()).thenReturn(customers);

        mockMvc.perform(get(CustomerController.BASE_URL)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customers", hasSize(2)));
    }

    @Test
    public void testGetCustomerById() throws Exception {

        //given
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setFirstName(FIRSTNAME1);
        customerDTO1.setLastName(LASTNAME1);
        customerDTO1.setCustomerUrl(URL1);

        when(customerService.getById(anyLong())).thenReturn(customerDTO1);

        //when
        mockMvc.perform(get(URL1)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo(FIRSTNAME1)));
    }

    @Test
    public void testGetCustomerByIdNotFound() throws Exception {

        when(customerService.getById(anyLong())).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get(URL1)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreateNewCustomer() throws Exception {

        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setFirstName(FIRSTNAME1);
        customerDTO1.setLastName(LASTNAME1);

        CustomerDTO returnedCustomerDTO = new CustomerDTO();
        returnedCustomerDTO.setFirstName(customerDTO1.getFirstName());
        returnedCustomerDTO.setLastName(customerDTO1.getLastName());
        returnedCustomerDTO.setCustomerUrl(URL1);

        when(customerService.saveNewCustomer(any(CustomerDTO.class))).thenReturn(returnedCustomerDTO);

        mockMvc.perform(post(CustomerController.BASE_URL)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(customerDTO1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", equalTo(returnedCustomerDTO.getFirstName())))
                .andExpect(jsonPath("$.customerUrl", equalTo(returnedCustomerDTO.getCustomerUrl())));
    }

    @Test
    void testUpdateCustomer() throws Exception {
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setFirstName(FIRSTNAME1);
        customerDTO1.setLastName(LASTNAME1);

        CustomerDTO returnedCustomerDTO = new CustomerDTO();
        returnedCustomerDTO.setFirstName(customerDTO1.getFirstName());
        returnedCustomerDTO.setLastName(customerDTO1.getLastName());
        returnedCustomerDTO.setCustomerUrl(URL1);

        when(customerService.updateCustomer(anyLong(), any(CustomerDTO.class))).thenReturn(returnedCustomerDTO);

        mockMvc.perform(put(URL1)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(customerDTO1)))
                .andReturn().getResponse().getContentAsString();
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.firstName", equalTo(FIRSTNAME1)))
//                .andExpect(jsonPath("$.customerUrl", equalTo(URL1)));
    }

    @Test
    void testPatchCustomer() throws Exception {
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setFirstName(UPDATED_NAME);

        CustomerDTO updatedCustomerDTO1 = new CustomerDTO();
        updatedCustomerDTO1.setFirstName(customerDTO1.getFirstName());
        updatedCustomerDTO1.setLastName(LASTNAME1);
        updatedCustomerDTO1.setCustomerUrl(URL1);

        when(customerService.patchCustomer(anyLong(), any(CustomerDTO.class))).thenReturn(updatedCustomerDTO1);

        mockMvc.perform(patch(URL1)
                .accept(APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(customerDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(UPDATED_NAME)))
                .andExpect(jsonPath("$.lastName", is(LASTNAME1)))
                .andExpect(jsonPath("$.customerUrl", is(URL1)));
    }

    @Test
    void testDeleteCustomer() throws Exception {
        mockMvc.perform(delete(URL1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(customerService, times(1)).deleteById(anyLong());
    }
}