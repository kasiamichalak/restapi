package pl.casmic.restapi.controllers.v1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.casmic.restapi.api.v1.model.VendorDTO;
import pl.casmic.restapi.services.ResourceNotFoundException;
import pl.casmic.restapi.services.VendorService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.casmic.restapi.controllers.v1.AbstractRestControllerTest.asJSONString;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = VendorController.class)
class VendorControllerTest {

    @MockBean
    VendorService vendorService;
    @Autowired
    MockMvc mockMvc;

    VendorDTO vendorDTO1;
    VendorDTO vendorDTO2;

    @BeforeEach
    void setUp() {
        vendorDTO1 = new VendorDTO("Name1", VendorController.BASE_URL + "/1");
        vendorDTO2 = new VendorDTO("Name2", VendorController.BASE_URL + "/2");
    }

    @Test
    void testGetAllVendors() throws Exception {
    //given
        List<VendorDTO> vendorDTOS = Arrays.asList(vendorDTO1, vendorDTO2);
        given(vendorService.getAllVendors()).willReturn(vendorDTOS);
    //when and then
        mockMvc.perform(get(VendorController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.vendors", hasSize(2)));
    }

    @Test
    void testGetVendorById() throws Exception {

        given(vendorService.getVendorById(anyLong())).willReturn(vendorDTO1);

        mockMvc.perform(get(vendorDTO1.getVendorUrl())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(vendorDTO1.getName())));
    }

    @Test
    void testGetVendorByIdWhenNotFound() throws Exception {

        given(vendorService.getVendorById(anyLong())).willThrow(ResourceNotFoundException.class);

        mockMvc.perform(get(vendorDTO1.getVendorUrl())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testSaveNewVendor() throws Exception {

        given(vendorService.saveNewVendor(any(VendorDTO.class))).willReturn(vendorDTO1);

        mockMvc.perform(post(VendorController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(vendorDTO1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", equalTo(vendorDTO1.getName())));
    }

    @Test
    void testUpdateVendor() throws Exception {

        given(vendorService.updateVendor(anyLong(), any(VendorDTO.class))).willReturn(vendorDTO1);

        mockMvc.perform(put(vendorDTO1.getVendorUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(vendorDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(vendorDTO1.getName())));
    }

    @Test
    void testPatchVendor() throws Exception {

        given(vendorService.patchVendor(anyLong(), any(VendorDTO.class))).willReturn(vendorDTO1);

        mockMvc.perform(patch(vendorDTO1.getVendorUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJSONString(vendorDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(vendorDTO1.getName())));
    }

    @Test
    void testDeleteVendor() throws Exception {

        mockMvc.perform(delete(vendorDTO1.getVendorUrl())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    // 'should' defaults to times = 1
        then(vendorService).should().deleteVendorById(anyLong());
    }
}