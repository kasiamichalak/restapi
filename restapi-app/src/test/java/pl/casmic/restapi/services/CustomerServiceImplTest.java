package pl.casmic.restapi.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.casmic.restapi.api.v1.mapper.CustomerMapper;
import pl.casmic.restapi.model.CustomerDTO;
import pl.casmic.restapi.controllers.v1.CustomerController;
import pl.casmic.restapi.domain.Customer;
import pl.casmic.restapi.repositories.CustomerRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class CustomerServiceImplTest {

    public static final long ID = 1L;
    public static final String FIRSTNAME = "Firstname";
    public static final String LASTNAME = "Lastname";
    public static final String URL = CustomerController.BASE_URL + "/" + ID;

    @Mock
    CustomerRepository customerRepository;
    CustomerService customerService;
    CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        customerService = new CustomerServiceImpl(customerRepository, customerMapper);
    }


    @Test
    void testGetAllCustomers() {
        List<Customer> customers = Arrays.asList(new Customer(), new Customer());
        when(customerRepository.findAll()).thenReturn(customers);

        List<CustomerDTO> customerDTOS = customerService.getAllCustomers();

        assertEquals(customerDTOS.size(), customers.size());
    }

    @Test
    public void testGetById() {
        Customer customer = new Customer();
        customer.setId(ID);
        customer.setFirstName(FIRSTNAME);
        customer.setLastName(LASTNAME);
        when(customerRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(customer));

        CustomerDTO customerDTO = customerService.getById(ID);

        assertEquals(FIRSTNAME, customerDTO.getFirstName());
        assertEquals(LASTNAME, customerDTO.getLastName());
    }

    @Test
    void testSaveCustomer() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(FIRSTNAME);
        customerDTO.setLastName(LASTNAME);
        customerDTO.setCustomerUrl(URL);

        Customer customer = new Customer();
        customer.setFirstName(customerDTO.getFirstName());
        customer.setLastName(customerDTO.getLastName());
        customer.setId(ID);

        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerDTO savedCustomerDTO = customerService.saveNewCustomer(customerDTO);

        assertEquals(customerDTO.getFirstName(), savedCustomerDTO.getFirstName());
        assertEquals(customerDTO.getLastName(), savedCustomerDTO.getLastName());
        assertEquals(URL, savedCustomerDTO.getCustomerUrl());
    }

    @Test
    void testUpdateCustomer() {

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(FIRSTNAME);
        customerDTO.setLastName(LASTNAME);

        Customer customer = new Customer();
        customer.setFirstName(customerDTO.getFirstName());
        customer.setLastName(customerDTO.getLastName());
        customer.setId(ID);

        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerDTO savedCustomerDTO = customerService.updateCustomer(ID, customerDTO);

        assertEquals(customerDTO.getFirstName(), savedCustomerDTO.getFirstName());
        assertEquals(customerDTO.getLastName(), savedCustomerDTO.getLastName());
        assertEquals(URL, savedCustomerDTO.getCustomerUrl());
    }

    @Test
    void testDeleteById() {
        customerService.deleteById(ID);
        verify(customerRepository, times(1)).deleteById(anyLong());
    }
}