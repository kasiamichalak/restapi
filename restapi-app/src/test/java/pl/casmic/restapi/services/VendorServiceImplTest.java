package pl.casmic.restapi.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.casmic.restapi.api.v1.mapper.VendorMapper;
import pl.casmic.restapi.api.v1.model.VendorDTO;
import pl.casmic.restapi.controllers.v1.VendorController;
import pl.casmic.restapi.domain.Vendor;
import pl.casmic.restapi.repositories.VendorRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

class VendorServiceImplTest {

    public static final Long ID = 1L;
    public static final String NAME = "Name";
    public static final String NAME_UPDATED = "Name Updated";
    public static final String URL = VendorController.BASE_URL + "/" + ID;

    @Mock
    VendorRepository vendorRepository;
    VendorService vendorService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        vendorService = new VendorServiceImpl(vendorRepository, VendorMapper.INSTANCE);
    }

    @Test
    void testGetAllVendors() {

    //BDDmockito library (given, when, then)
    //given
        List<Vendor> vendors = Arrays.asList(new Vendor(), new Vendor());
        given(vendorRepository.findAll()).willReturn(vendors);
    //when
        List<VendorDTO> vendorDTOS = vendorService.getAllVendors();
    //then
        then(vendorRepository).should(times(1)).findAll();
        Assertions.assertEquals(vendorDTOS.size(), vendors.size());
    }

    @Test
    void testGetVendorById() {
    //given
        Vendor vendor = getVendor();
        given(vendorRepository.findById(anyLong())).willReturn(Optional.of(vendor));
    //when
        VendorDTO vendorDTO = vendorService.getVendorById(ID);
    //then
    // 'should' defaults to times = 1
        then(vendorRepository).should().findById(anyLong());
        Assertions.assertEquals(vendor.getName(), vendorDTO.getName());
    }

    @Test
    void testGetVendorByIdWhenNotFound() throws Exception {
    //given
        given(vendorRepository.findById(anyLong())).willReturn(Optional.empty());
    //when and then
        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> vendorService.getVendorById(ID));
        then(vendorRepository).should().findById(anyLong());
    }

    @Test
    void testCreateNewVendor() {
    //given
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(NAME);
        vendorDTO.setVendorUrl(URL);

        Vendor vendor = getVendor();
        given(vendorRepository.save(any(Vendor.class))).willReturn(vendor);
    //when
        VendorDTO savedVendorDTO = vendorService.saveNewVendor(vendorDTO);
    //then
        then(vendorRepository).should().save(any(Vendor.class));
        Assertions.assertEquals(vendorDTO.getName(), savedVendorDTO.getName());
        Assertions.assertEquals(vendorDTO.getVendorUrl(), savedVendorDTO.getVendorUrl());
    }

    @Test
    void testUpdateVendor() {
   //given
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(NAME);
        vendorDTO.setVendorUrl(URL);

        Vendor vendor = getVendor();
        given(vendorRepository.save(any(Vendor.class))).willReturn(vendor);
    //when
        VendorDTO updatedVendorDTO = vendorService.updateVendor(ID, vendorDTO);
    //then
        then(vendorRepository).should((times(1))).save(any(Vendor.class));
        Assertions.assertEquals(vendorDTO.getName(), updatedVendorDTO.getName());
        Assertions.assertEquals(vendorDTO.getVendorUrl(), updatedVendorDTO.getVendorUrl());
    }

    @Test
    void testPatchVendor() {
        //given
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(NAME_UPDATED);
        vendorDTO.setVendorUrl(URL);

        Vendor vendor = getVendor();
        given(vendorRepository.findById(anyLong())).willReturn(Optional.of(vendor));
    //when
        VendorDTO updatedVendorDTO = vendorService.patchVendor(ID, vendorDTO);
    //then
        then(vendorRepository).should(times(1)).findById(anyLong());
        Assertions.assertEquals(vendorDTO.getName(), updatedVendorDTO.getName());
        Assertions.assertEquals(vendorDTO.getVendorUrl(), updatedVendorDTO.getVendorUrl());
    }

    @Test
    void testDeleteVendor() {
    //when
        vendorService.deleteVendorById(ID);
    //then
        then(vendorRepository).should().deleteById(anyLong());
    }

    private Vendor getVendor() {
        Vendor vendor = new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);
        return vendor;
    }
}