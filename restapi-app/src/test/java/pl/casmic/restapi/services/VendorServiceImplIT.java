package pl.casmic.restapi.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.casmic.restapi.api.v1.mapper.CustomerMapper;
import pl.casmic.restapi.api.v1.mapper.VendorMapper;
import pl.casmic.restapi.api.v1.model.CustomerDTO;
import pl.casmic.restapi.api.v1.model.VendorDTO;
import pl.casmic.restapi.bootstrap.Bootstrap;
import pl.casmic.restapi.domain.Customer;
import pl.casmic.restapi.domain.Vendor;
import pl.casmic.restapi.repositories.CategoryRepository;
import pl.casmic.restapi.repositories.CustomerRepository;
import pl.casmic.restapi.repositories.VendorRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class VendorServiceImplIT {

    public static final String UPDATED_NAME = "UpdatedName";

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    VendorRepository vendorRepository;

    VendorService vendorService;

    @BeforeEach
    void setUp() throws Exception {
        System.out.println("Loading Vendor Data");
        System.out.println(vendorRepository.findAll().size());

        Bootstrap bootstrap = new Bootstrap(categoryRepository, customerRepository, vendorRepository);
        bootstrap.run();

        vendorService = new VendorServiceImpl(vendorRepository, VendorMapper.INSTANCE);
    }

    @Test
    void testPatchVendorUpdateName() {

        Long id = getVendorIdValue();

        Vendor originalVendor = vendorRepository.getOne(id);
        assertNotNull(originalVendor);
        String originalName = originalVendor.getName();
        
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(UPDATED_NAME);

        vendorService.patchVendor(id, vendorDTO);

        Vendor updatedVendor = vendorRepository.findById(id).get();

        assertNotNull(updatedVendor);
        assertEquals(UPDATED_NAME, updatedVendor.getName());
        assertNotEquals(originalName, updatedVendor.getName());
    }

    private Long getVendorIdValue(){
        List<Vendor> vendors = vendorRepository.findAll();
        System.out.println("Vendors Found: " + vendors.size());
        return vendors.get(0).getId();
    }
}