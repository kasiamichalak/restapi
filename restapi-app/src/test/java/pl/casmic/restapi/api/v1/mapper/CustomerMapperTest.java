package pl.casmic.restapi.api.v1.mapper;

import org.junit.jupiter.api.Test;
import pl.casmic.restapi.model.CustomerDTO;
import pl.casmic.restapi.domain.Customer;

import static org.junit.jupiter.api.Assertions.*;

class CustomerMapperTest {

    public static final Long ID = 1L;
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";

    CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @Test
    void testCustomerToCustomerDTO() {

        Customer customer = new Customer();
        customer.setId(ID);
        customer.setFirstName(FIRSTNAME);
        customer.setLastName(LASTNAME);

        CustomerDTO customerDTO = customerMapper.customerToCustomerDTO(customer);

        assertEquals(customerDTO.getFirstName(), FIRSTNAME);
        assertEquals(customerDTO.getLastName(), LASTNAME);

    }

    @Test
    void testCustomerDTOToCustomer() {

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(FIRSTNAME);
        customerDTO.setLastName(LASTNAME);

        Customer customer = customerMapper.customerDTOToCustomer(customerDTO);

        assertEquals(customer.getFirstName(), FIRSTNAME);
        assertEquals(customer.getLastName(), LASTNAME);
    }
}