package pl.casmic.restapi.api.v1.mapper;

import org.junit.jupiter.api.Test;
import pl.casmic.restapi.api.v1.model.VendorDTO;
import pl.casmic.restapi.domain.Vendor;

import static org.junit.jupiter.api.Assertions.*;

class VendorMapperTest {

    public static final Long ID = 1L;
    public static final String NAME = "Name";
    VendorMapper vendorMapper = VendorMapper.INSTANCE;


    @Test
    void testVendorToVendorDTO() {

        Vendor vendor = new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);

        VendorDTO vendorDTO = vendorMapper.vendorToVendorDTO(vendor);

        assertEquals(NAME, vendorDTO.getName());
    }

    @Test
    void testVendorDTOToVendor() {

        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(NAME);

        Vendor vendor = vendorMapper.vendorDTOToVendor(vendorDTO);

        assertEquals(NAME, vendor.getName());
    }
}