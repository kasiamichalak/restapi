package pl.casmic.restapi.api.v1.mapper;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import pl.casmic.restapi.api.v1.model.CategoryDTO;
import pl.casmic.restapi.domain.Category;

import static org.junit.jupiter.api.Assertions.*;

class CategoryMapperTest {

    public static final long ID = 1L;
    public static final String NAME = "Name";
    CategoryMapper categoryMapper = CategoryMapper.INSTANCE;

    @Test
    void testCategoryToCategoryDTO() {

        Category category = new Category();
        category.setId(ID);
        category.setName(NAME);

        CategoryDTO categoryDTO = categoryMapper.categoryToCategoryDTO(category);

        assertEquals(ID, categoryDTO.getId());
        assertEquals(NAME, categoryDTO.getName());

    }
}