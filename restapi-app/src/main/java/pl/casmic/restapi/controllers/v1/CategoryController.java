package pl.casmic.restapi.controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.casmic.restapi.api.v1.model.CategoryDTO;
import pl.casmic.restapi.api.v1.model.CategoryListDTO;
import pl.casmic.restapi.services.CategoryService;

@Api("this is Category Controller")
@RestController
@RequestMapping(CategoryController.BASE_URL)
public class CategoryController {

    public static final String BASE_URL = "/api/v1/categories";

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation(value = "this will list all categories", notes = "api notes")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CategoryListDTO getCategories() {
        return new CategoryListDTO(categoryService.getAllCategories());
    }

    @GetMapping("{name}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryDTO getByName(@PathVariable String name) {
        return categoryService.getByName(name);
    }
}
