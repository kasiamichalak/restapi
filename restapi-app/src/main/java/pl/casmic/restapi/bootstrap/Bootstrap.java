package pl.casmic.restapi.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.casmic.restapi.domain.Category;
import pl.casmic.restapi.domain.Customer;
import pl.casmic.restapi.domain.Vendor;
import pl.casmic.restapi.repositories.CategoryRepository;
import pl.casmic.restapi.repositories.CustomerRepository;
import pl.casmic.restapi.repositories.VendorRepository;

import java.util.Arrays;

@Component
public class Bootstrap implements CommandLineRunner {

    private CategoryRepository categoryRepository;
    private CustomerRepository customerRepository;
    private VendorRepository vendorRepository;

    public Bootstrap(CategoryRepository categoryRepository,
                     CustomerRepository customerRepository,
                     VendorRepository vendorRepository) {
        this.categoryRepository = categoryRepository;
        this.customerRepository = customerRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        loadCategories();
        loadCustomers();
        loadVendors();
    }

    private void loadCustomers() {
        Customer customer1 = new Customer();
        customer1.setFirstName("John");
        customer1.setLastName("Doe");

        Customer customer2 = new Customer();
        customer2.setFirstName("Jimmy");
        customer2.setLastName("Choo");

        Customer customer3 = new Customer();
        customer3.setFirstName("Amanda");
        customer3.setLastName("Jonse");

        customerRepository.saveAll(Arrays.asList(customer1, customer2, customer3));

        System.out.println("Customer Data Loaded: " + customerRepository.count());
    }

    private void loadCategories() {
        Category fruits = new Category();
        fruits.setName("Fruits");

        Category dried = new Category();
        dried.setName("Dried");

        Category exotic = new Category();
        exotic.setName("Exotic");

        Category nuts = new Category();
        nuts.setName("Nuts");

        Category fresh = new Category();
        fresh.setName("Fresh");

        Category rotten = new Category();
        rotten.setName("Rotten");

        categoryRepository.save(fruits);
        categoryRepository.save(dried);
        categoryRepository.save(exotic);
        categoryRepository.save(nuts);
        categoryRepository.save(fresh);
        categoryRepository.save(rotten);

        System.out.println("Category Data Loaded: " + categoryRepository.count());
    }

    private void loadVendors() {

        Vendor vendor1 = new Vendor();
        vendor1.setId(1L);
        vendor1.setName("Fresh Fruits Ltd");

        Vendor vendor2 = new Vendor();
        vendor2.setId(2L);
        vendor2.setName("Banana Maniana GmbH");

        Vendor vendor3 = new Vendor();
        vendor3.setId(3L);
        vendor3.setName("Oranges & Apples Company");

        vendorRepository.save(vendor1);
        vendorRepository.save(vendor2);
        vendorRepository.save(vendor3);

        System.out.println("Vendor Data Loaded: " + vendorRepository.count());
    }
}
