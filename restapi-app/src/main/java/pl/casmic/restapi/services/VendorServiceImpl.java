package pl.casmic.restapi.services;

import org.springframework.stereotype.Service;
import pl.casmic.restapi.api.v1.mapper.VendorMapper;
import pl.casmic.restapi.api.v1.model.VendorDTO;
import pl.casmic.restapi.controllers.v1.VendorController;
import pl.casmic.restapi.domain.Vendor;
import pl.casmic.restapi.repositories.VendorRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VendorServiceImpl implements VendorService {

    VendorRepository vendorRepository;
    VendorMapper vendorMapper;

    public VendorServiceImpl(VendorRepository vendorRepository,
                             VendorMapper vendorMapper) {
        this.vendorRepository = vendorRepository;
        this.vendorMapper = vendorMapper;
    }

    @Override
    public List<VendorDTO> getAllVendors() {
        return vendorRepository.findAll().stream()
                .map(this::returnVendorDTOWithUrl)
                .collect(Collectors.toList());
    }

    @Override
    public VendorDTO getVendorById(Long id) {
        return vendorRepository.findById(id)
                .map(this::returnVendorDTOWithUrl)
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public VendorDTO saveNewVendor(VendorDTO vendorDTO) {
        Vendor savedVendor = vendorRepository.save(vendorMapper.vendorDTOToVendor(vendorDTO));
        return returnVendorDTOWithUrl(savedVendor);
    }

    @Override
    public VendorDTO updateVendor(Long id, VendorDTO vendorDTO) {
        Vendor vendor = vendorMapper.vendorDTOToVendor(vendorDTO);
        vendor.setId(id);
        Vendor savedVendor = vendorRepository.save(vendor);
        return returnVendorDTOWithUrl(savedVendor);
    }

    @Override
    public VendorDTO patchVendor(Long id, VendorDTO vendorDTO) {
        return vendorRepository.findById(id)
                .map(vendor -> {
                    if (vendorDTO.getName() != null) {
                        vendor.setName(vendorDTO.getName());
                    }
                    vendorRepository.save(vendor);
                    return returnVendorDTOWithUrl(vendor);
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public void deleteVendorById(Long id) {
        vendorRepository.deleteById(id);
    }

    private VendorDTO returnVendorDTOWithUrl(Vendor vendor) {
        VendorDTO updatedVendorDTO = vendorMapper.vendorToVendorDTO(vendor);
        updatedVendorDTO.setVendorUrl(VendorController.BASE_URL + "/" + vendor.getId());
        return updatedVendorDTO;
    }
}
