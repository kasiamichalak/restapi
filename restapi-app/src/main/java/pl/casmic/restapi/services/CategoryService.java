package pl.casmic.restapi.services;

import pl.casmic.restapi.api.v1.model.CategoryDTO;

import java.util.List;

public interface CategoryService {

    List<CategoryDTO> getAllCategories();
    CategoryDTO getByName(String name);

}
