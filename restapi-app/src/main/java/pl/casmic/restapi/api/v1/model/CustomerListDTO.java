package pl.casmic.restapi.api.v1.model;

import pl.casmic.restapi.model.CustomerDTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CustomerListDTO {

    private List<CustomerDTO> customers;
}
