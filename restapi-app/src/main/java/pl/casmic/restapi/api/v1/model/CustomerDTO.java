package pl.casmic.restapi.api.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CustomerDTO {
    @ApiModelProperty(value = "this is the first name of customer", required = true)
    private String firstName;
    private String lastName;
    @JsonProperty("customer_url")
    private String customerUrl;

}
