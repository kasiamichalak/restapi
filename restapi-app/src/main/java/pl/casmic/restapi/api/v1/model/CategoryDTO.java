package pl.casmic.restapi.api.v1.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CategoryDTO {

    private Long id;
    private String name;
}
