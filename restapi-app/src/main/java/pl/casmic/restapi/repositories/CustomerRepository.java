package pl.casmic.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.casmic.restapi.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
