package pl.casmic.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.casmic.restapi.domain.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Long> {

}
