package pl.casmic.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.casmic.restapi.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findByName(String name);
}
